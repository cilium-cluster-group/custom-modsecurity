FROM alpine

COPY ./script.sh /usr/local/bin

CMD ["/bin/sh", "-c", "/usr/local/bin/script.sh"]
