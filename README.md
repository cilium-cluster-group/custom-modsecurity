## WAF Enablement Demo

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).


It demonstrates our new [Web Application Firewall (WAF)](https://docs.gitlab.com/ee/user/clusters/applications.html#web-application-firewall-modsecurity)
capability [introduced in %12.4](https://gitlab.com/gitlab-org/gitlab/issues/25398).

To view the logs in the default non-blocking mode, tail the audit log and make an insecure request.

### Viewing WAF Logs

Logs are viewable by tailing the modsecurity audit log for those who have permission to access the `waf-enablement-demo` cluster.

To view logs for production environment one must have GCP project access and both `google-sdk` and `kubectl` installed. Then run the following commands locally:

```shell
$ gcloud container clusters get-credentials waf-enablement-demo --zone us-central1-a --project group-defend-c8e44e

$ kubectl -n gitlab-managed-apps exec -it $(kubectl get pods -n gitlab-managed-apps | grep 'ingress-controller' | awk '{print $1}') -- tail -f /var/log/modsec/audit.log
```

### Triggering insecure request to WAF

Insecure inputs can be specified [by visiting the environment](https://gitlab-org-defend-waf-enablement-demo.35.239.17.16.sslip.io/) or via curl; i.e.

```shell
curl \
  -k \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  --data 'name=%3Cscript%3Ealert%28%27hello%27%29%3C%2Fscript%3E' \
  'https://gitlab-org-defend-waf-enablement-demo.35.239.17.16.sslip.io/greetings'
```

## CI/CD with Auto DevOps

This project is deployed using [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).
